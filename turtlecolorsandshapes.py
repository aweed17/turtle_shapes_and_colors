#Aaron Weed
#shapesandcolors
#November,11 2016


import turtle as T


def circle(color,size): #user chose to draw a circle
    T.Screen() #print to screen
    T.color(color) #user has option to choose color of shape, choosing color
    T.circle(50) #drawing the circle
    T.done


def square(color,size): #user chose a square to draw
    T.Screen() #printing to screen
    T.color(color) #user choosing color of shape, and program recognizing it
    for i in range (4): # commands for drawing the square
        T.forward(size)
        T.right(90)

    T.done()

def star(color,size): #user chose to draw this star
    T.Screen() # printing to screen
    T.color(color) #user choosing color of shape, program printing/recognizing
    for i in range(5): #commands for drawing the star
        T.forward(size)
        T.right(144)
    T.done()


def triangle(color,size): #user chose to draw this triangle in program
    T.Screen() #printing the triangle to the screen
    T.color(color) #color chosen for the triangle and printing color
    for i in range(3): #commands for drawing the triangle
        T.forward(size)
        T.right(120)
    T.done()

print ("You can draw a circle, square, triangle, or star") #showing options for
#program
print("what shape would you like to draw?") #asking user which shape they want
shapeinput = input()
shapeinput = shapeinput.lower()
#print("{}".format(userinput))

print("what color do you want?") #asking user what color they would like
colorInput = input()
colorInput = colorInput.lower()
print("what size would you like your shape to be?")#givng user a size option
print("small, medium, or large")

sizeinput = input().lower()

small = 50 #size options for what pixel size shape they would like
medium = 150
large = 250


if sizeinput == "small": #print commands for size in shape
    sizeinput = 50

if sizeinput == "medium":
    sizeinput = 150

if sizeinput == "large":
    sizeinput = 250




if shapeinput == "square": #print commands for what shape user chose
    square(colorInput,sizeinput)


elif shapeinput == "triangle":
    triangle(colorInput,sizeinput)

elif shapeinput == "circle":
    circle(colorInput,sizeinput)

elif shapeinput == "star":
    star(colorInput,sizeinput)

else:
    print("you did not choose one of the choices for shapes!")
#telling user they didn't choose a correct option for a shape




print("shape = {}".format(shapeinput))

